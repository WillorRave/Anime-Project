package com.example.animeapp.models.animeModel


import com.google.gson.annotations.SerializedName

data class Mappings(
    @SerializedName("links")
    val links: LinksX
)
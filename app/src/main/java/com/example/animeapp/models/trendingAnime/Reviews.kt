package com.example.animeapp.models.trendingAnime


import com.google.gson.annotations.SerializedName

data class Reviews(
    @SerializedName("links")
    val links: LinksX
)
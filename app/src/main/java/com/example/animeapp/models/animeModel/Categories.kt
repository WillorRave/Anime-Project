package com.example.animeapp.models.animeModel


import com.google.gson.annotations.SerializedName

data class Categories(
    @SerializedName("links")
    val links: LinksX
)
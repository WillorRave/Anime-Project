package com.example.animeapp.models.trendingAnime


import com.google.gson.annotations.SerializedName

data class Installments(
    @SerializedName("links")
    val links: LinksX
)
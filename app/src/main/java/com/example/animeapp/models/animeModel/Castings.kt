package com.example.animeapp.models.animeModel


import com.google.gson.annotations.SerializedName

data class Castings(
    @SerializedName("links")
    val links: LinksX
)
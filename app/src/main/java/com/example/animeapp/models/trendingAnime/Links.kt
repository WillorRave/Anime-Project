package com.example.animeapp.models.trendingAnime


import com.google.gson.annotations.SerializedName

data class Links(
    @SerializedName("self")
    val self: String
)
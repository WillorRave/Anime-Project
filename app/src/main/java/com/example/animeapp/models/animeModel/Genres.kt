package com.example.animeapp.models.animeModel


import com.google.gson.annotations.SerializedName

data class Genres(
    @SerializedName("links")
    val links: LinksX
)
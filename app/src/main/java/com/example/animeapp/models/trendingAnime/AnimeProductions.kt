package com.example.animeapp.models.trendingAnime


import com.google.gson.annotations.SerializedName

data class AnimeProductions(
    @SerializedName("links")
    val links: LinksX
)
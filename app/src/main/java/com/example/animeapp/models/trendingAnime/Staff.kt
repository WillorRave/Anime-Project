package com.example.animeapp.models.trendingAnime


import com.google.gson.annotations.SerializedName

data class Staff(
    @SerializedName("links")
    val links: LinksX
)
package com.example.animeapp.models.animeModel


import com.google.gson.annotations.SerializedName

data class SingleAnimeData(
    @SerializedName("data")
    val `data`: Data
)
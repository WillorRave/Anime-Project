package com.example.animeapp.models.animeModel


import com.google.gson.annotations.SerializedName

data class AnimeProductions(
    @SerializedName("links")
    val links: LinksX
)
package com.example.animeapp.models.trendingAnime


import com.google.gson.annotations.SerializedName

data class AnimeDataList(
    @SerializedName("data")
    val `data`: List<Data>
)
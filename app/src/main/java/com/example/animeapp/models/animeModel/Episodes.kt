package com.example.animeapp.models.animeModel


import com.google.gson.annotations.SerializedName

data class Episodes(
    @SerializedName("links")
    val links: LinksX
)
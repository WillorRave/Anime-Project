package com.example.animeapp.models.trendingAnime


import com.google.gson.annotations.SerializedName

data class AnimeCharacters(
    @SerializedName("links")
    val links: LinksX
)
package com.example.animeapp.models.animeModel


import com.google.gson.annotations.SerializedName

data class Staff(
    @SerializedName("links")
    val links: LinksX
)
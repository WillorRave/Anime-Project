package com.example.animeapp.models.trendingAnime


import com.google.gson.annotations.SerializedName

data class AnimeStaff(
    @SerializedName("links")
    val links: LinksX
)
package com.example.animeapp.models.animeModel


import com.google.gson.annotations.SerializedName

data class AnimeStaff(
    @SerializedName("links")
    val links: LinksX
)
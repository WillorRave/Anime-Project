package com.example.animeapp.models.animeModel


import com.google.gson.annotations.SerializedName

data class Links(
    @SerializedName("self")
    val self: String
)
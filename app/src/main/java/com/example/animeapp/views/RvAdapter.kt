package com.example.animeapp.views

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.animeapp.databinding.RvAnimeItemBinding
import com.example.animeapp.models.trendingAnime.Data
import com.example.animeapp.screens.MainFragmentDirections

class RvAdapter: RecyclerView.Adapter<RvAdapter.RvAnimeDisplayItem>(){

    private lateinit var animeDataList: List<Data>

    inner class RvAnimeDisplayItem(val binding: RvAnimeItemBinding
    ): RecyclerView.ViewHolder(binding.root){

        lateinit var animeData: Data

        fun setUpView(animeData: Data){
            this.animeData = animeData
            with(binding){
                imgbtnAnimeImage.load(animeData.attributes.posterImage.small)
                tvAnimeTitle.text = animeData.attributes.canonicalTitle
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RvAnimeDisplayItem {
        val binding = RvAnimeItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return RvAnimeDisplayItem(binding)
    }

    override fun onBindViewHolder(holder: RvAnimeDisplayItem, position: Int) {

        holder.setUpView(animeDataList[position])

        with(holder.binding){
            imgbtnAnimeImage.setOnClickListener {

                it.findNavController().navigate(
                    MainFragmentDirections.actionMainFragmentToDetailsFragment(
                        holder.animeData.id
                    )
                )
            }
        }

    }

    override fun getItemCount(): Int {
        return animeDataList.size
    }

    fun setUpAnimeList(animeList: List<Data>){
        this.animeDataList = animeList
    }
}
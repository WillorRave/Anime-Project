package com.example.animeapp.kitsuAPI

import com.example.animeapp.models.animeModel.SingleAnimeData
import com.example.animeapp.models.trendingAnime.AnimeDataList
import retrofit2.Response

object KitsuApiImplentation: KitsuApiSkeleton{

    val api by lazy{
        KitsuApiSkeleton.getInstanceOfApi().create(KitsuApiSkeleton::class.java)
    }



    override suspend fun getTrendingAnime(): Response<AnimeDataList> {
        val response = api.getTrendingAnime()
        return response
    }

    override suspend fun getAnimeDetails(id: String): Response<SingleAnimeData> {
        val response = api.getAnimeDetails(id)

        // Testing
//        val details = response.body()?.data?.attributes?.


        return response
    }


}
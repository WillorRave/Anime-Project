package com.example.animeapp.kitsuAPI

import com.example.animeapp.models.animeModel.SingleAnimeData
import com.example.animeapp.models.trendingAnime.AnimeDataList
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface KitsuApiSkeleton {

    @GET("edge/trending/anime")
    suspend fun getTrendingAnime(): Response<AnimeDataList>

    @GET("edge/anime/{id}")
    suspend fun getAnimeDetails(@Path("id") id: String):Response<SingleAnimeData>

//    @GET("blah/blah/{animeId}")
//    // Note...To pass parameters to the url you need to use the @Query(param_name) syntax
//    suspend fun getInfoForAnime(@Query())


    companion object{
        const val baseUrl = "https://kitsu.io/api/"

        fun getInstanceOfApi(): Retrofit{
            return Retrofit.Builder()   // Uses the "Builder Design Model"
                .baseUrl(baseUrl)       // Attach the 'baseUrl' to our Retrofit Object

                // Used to convert Json to Data objects
                .addConverterFactory(GsonConverterFactory.create())

                .build()                // Call to actually build Retrofit object
        }

    }

}
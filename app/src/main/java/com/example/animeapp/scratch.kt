package com.example.animeapp

import com.example.animeapp.kitsuAPI.KitsuApiImplentation
import com.example.animeapp.kitsuAPI.KitsuApiSkeleton
import kotlinx.coroutines.runBlocking

fun main() {
    val repo = KitsuApiImplentation.api

    runBlocking {
        val resp = repo.getTrendingAnime()
        println(resp.body()?.data?.get(2)?.attributes?.posterImage)
        }
    }


package com.example.animeapp.viewModels

import androidx.lifecycle.ViewModel
import com.example.animeapp.models.trendingAnime.AnimeDataList
import com.example.animeapp.models.trendingAnime.Data
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class MainFragViewModel: ViewModel() {


    private val _animeStateFlow = MutableStateFlow(listOf<Data>())
    val animeStateFlow = _animeStateFlow.asStateFlow()

    fun setUpStateFlows(animeDataObjects: AnimeDataList){
        var newList = mutableListOf<Data>()
        for(n in 0..animeDataObjects.data.size - 1){
            println(animeDataObjects.data[n].attributes.posterImage)
            newList.add(
                animeDataObjects.data[n]
            )
        }
        _animeStateFlow.value = newList
    }

}
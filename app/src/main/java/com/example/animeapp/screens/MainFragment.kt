package com.example.animeapp.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.example.animeapp.databinding.FragmentMainBinding
import com.example.animeapp.kitsuAPI.KitsuApiImplentation
import com.example.animeapp.viewModels.MainFragViewModel
import com.example.animeapp.views.RvAdapter
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class MainFragment: Fragment() {

    private lateinit var _binding: FragmentMainBinding
    private val binding get() = _binding

    private val vm = MainFragViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = FragmentMainBinding.inflate(inflater, container, false).also{
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews(){
        val repo = KitsuApiImplentation.api
        with(binding){
            // Get anime data
            lifecycleScope.launch{

                // Get data
                val response = repo.getTrendingAnime()

                vm.setUpStateFlows(response.body()!!)

                vm.animeStateFlow.collectLatest {
                    // Set up adapter
                    rvForAnime.adapter = RvAdapter().apply{
                        setUpAnimeList(it)
                    }
                }
            }
        }
    }

}
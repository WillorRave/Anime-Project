package com.example.animeapp.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import coil.load
import com.example.animeapp.databinding.FragmentDetailsBinding
import com.example.animeapp.databinding.FragmentMainBinding
import com.example.animeapp.kitsuAPI.KitsuApiImplentation
import com.example.animeapp.models.animeModel.SingleAnimeData
import kotlinx.coroutines.launch
import retrofit2.Response

class DetailsFragment: Fragment() {

    private lateinit var _binding : FragmentDetailsBinding
    private val binding get() = _binding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = FragmentDetailsBinding.inflate(inflater, container, false).also{
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val charId = arguments?.get("char_id").toString()

        lifecycleScope.launch(){
            val respons: Response<SingleAnimeData> = KitsuApiImplentation.api.getAnimeDetails(charId)
            val singleAnimeData = respons.body()?.data!!

            with(binding){
                ivTop.load(singleAnimeData.attributes.posterImage.original)
                tvTitle.text = singleAnimeData.attributes.canonicalTitle
                tvScore.text = "Score: " + singleAnimeData.attributes.averageRating
                tvRank.text = "Rank: " + singleAnimeData.attributes.ratingRank
                tvPopularity.text = "Popularity: " + singleAnimeData.attributes.popularityRank
                tvParagraph.text = singleAnimeData.attributes.synopsis
            }

        }

    }

}